
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

//features
import { BeginComponent } from './features/begin/begin.component';
import { DashboardComponent } from './features/dashboard/dashboard.component';

//components
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { SkeletonComponent } from './components/share/skeleton/skeleton.component';

const APP_ROUTES: Routes = [
  { path: 'begin',
  component: BeginComponent,
  children: [ // rutas hijas, se verán dentro del componente padre
        {
          path: 'login', // la ruta real es movimientos/nuevo
          component: LoginComponent
        },
        {
          path: 'register',
          component: RegisterComponent
        },
        {
           path: '**',
           pathMatch: 'full',
           redirectTo: 'login'
        }
    ]
  },
  { path: 'skeleton', component: SkeletonComponent },
  { path: 'dashboard', component: DashboardComponent },

  { path: '**',  pathMatch: 'full', redirectTo: 'begin'  }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
