
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

//features
import { BeginComponent } from './features/begin/begin.component';
import { DashboardComponent } from './features/dashboard/dashboard.component';

//components
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { NavbarComponent } from './components/share/navbar/navbar.component';

//PrimeNG
import {CardModule} from 'primeng/card';
import {ButtonModule} from 'primeng/button';
import {PasswordModule} from 'primeng/password';

//ROUTES
import { APP_ROUTING } from './app.routes';
import { SkeletonComponent } from './components/share/skeleton/skeleton.component';
import { SubNavbarComponent } from './components/share/navbar/sub-navbar/sub-navbar.component';

@NgModule({
  declarations: [
    AppComponent,
    BeginComponent,
    DashboardComponent,
    LoginComponent,
    RegisterComponent,
    NavbarComponent,
    SkeletonComponent,
    SubNavbarComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    CardModule,
    ButtonModule,
    APP_ROUTING
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
